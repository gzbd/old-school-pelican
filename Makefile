watch_scss:
	sass --watch -t compact --scss --sourcemap=none sass:static/css

scss:
	sass --update -t compact --scss --sourcemap=none sass:static/css

.PHONY: watch_scss
